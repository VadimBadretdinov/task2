//
//  UserRepo.swift
//  Task2
//
//  Created by Vadim Badretdinov on 27.10.2020.
//

import Foundation

struct UserRepo: Decodable {
    let id: Int
    let name: String
}
