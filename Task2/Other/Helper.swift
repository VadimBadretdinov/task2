//
//  Helper.swift
//  Task2
//
//  Created by Vadim Badretdinov on 28.10.2020.
//

import Foundation

enum Options: String {
    case help = "h"
    case quit = "q"
    case unknown

    init(option: String) {
        switch option {
        case "h": self = .help
        case "q": self = .quit
        default: self = .unknown
        }
    }
}

class Helper {

    private let networkManager = NetworkManager()

    func run(input: String) {
        let option = Options(option: input)

        switch option {
        case .help:
            print("Hi. Type github username to show all user repositories. \nType \"q\" to quit, \"h\" to see this message again.")
        case .quit:
            exit(EXIT_SUCCESS)
        default:
            networkManager.obtainUserRepositories(importArgs: input)
        }
    }
}
