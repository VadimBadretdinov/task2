//
//  NetworkManager.swift
//  Task2
//
//  Created by Vadim Badretdinov on 27.10.2020.
//

import Foundation
import Alamofire

final class NetworkManager {
    let queue = DispatchQueue(label: "Network", attributes: .concurrent)
    let group = DispatchGroup()

    func obtainUserRepositories(importArgs: String){
        let username = importArgs.replacingOccurrences(of: " ", with: "")
        group.enter()
        AF.request("https://api.github.com/users/\(username)/repos").responseDecodable(of: [UserRepo].self, queue: queue) { response in
            if let repos = response.value {
                print("User: \(username)\nRepositories:")
                repos.forEach({ print("-\($0.name)") })
            } else {
                print("Cannot find user: \(username)")
            }
            self.group.leave()
        }
    }
}
