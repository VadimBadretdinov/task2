//
//  main.swift
//  Task2
//
//  Created by Vadim Badretdinov on 27.10.2020.
//

import Foundation

let helper = Helper()
helper.run(input: "h")
while let input = readLine() {
    helper.run(input: input)
}
